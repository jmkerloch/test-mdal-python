# Tests utilisation package mdal-python pour création fichier mesh

Ce projet contient un script pour des tests de création de fichier supportés par MDAL.

Les fichiers sont créés avec le package pip mdal-python.

## Mise en place environnement

L'installation du package mdal-python nécessite d'une compilation de la librairie [MDAL](https://github.com/lutraconsulting/MDAL).

Les test ont été effectués sous Linux en respectant la [procédure d'installation de MDAL](https://github.com/lutraconsulting/MDAL#linux)

```
sudo apt-get install libgdal-dev libhdf5-dev libnetcdf-dev libxml2-dev

mkdir build;cd build
cmake -DCMAKE_BUILD_TYPE=Rel ..
make
sudo make install
```

Il est ensuite nécessaire de modifier la variable d'environnement pour rendre MDAL disponible:

`export LD_LIBRARY_PATH=/usr/local/lib`

Pour lancer le script:

- mise en place d'un environnement virtuel

`python -m venv .venv`

- activation environnement

`source .venv/bin/activate`

- installation mdal-python

`pip install -r requirements.txt`

## Résultat expérimentation

Des fichiers supportés par MDAL ont bien pu être créés avec le package mdal-python.

Les drivers suivant ont permis la création de fichier :

- 2DM
- PLY
- SELAFIN
- Ugrid

Ces fichiers sont correctement lus par MDAL dans QGIS.

Malgré une absence d'erreur pour l'ajout de données sur les noeuds pour le driver SELAFIN, aucune donnée n'est affichée dans QGIS.

:warning: Il n'a pas été possible d'ajouter une composante temporelle pour les fichiers .ply. Ceci n'est pas supporté par le driver PLY de MDAL. Une issue a été postée sur GitHub : https://github.com/lutraconsulting/MDAL/issues/461

Pour la création de fichier Ugrid, un script spécifique de test est disponible `create_ugrid.py`. Il a permis de démontrer la faisabilité de la création du maillage et de données sur les faces ou les vertices avec gestion d'une composante temporelle. Le fichier créé a pu être lu par QGIS. 

:warning: Lors de la lecture des fichiers par QGIS, une couche de données Bed elevation a automatiquement été ajouté par MDAL. Une issue a été postée sur GitHub : https://github.com/lutraconsulting/MDAL/issues/462
 
