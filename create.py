from mdal import PyMesh, drivers, MDAL_DataLocation
import numpy as np

for driver in drivers():

    test = PyMesh(driver)
    print(f"Mesh created by Driver : {test.driver_name}")

    # Create vertices and faces
    test.vertices=np.array([(0. , 0., 0.), (1. , 0., 0.), (2. , 0., 0.), (0.5, 1., 0.), (1.5, 1., 0.), (1. , 2., 0.)], dtype=[('X', '<f8'), ('Y', '<f8'), ('Z', '<f8')])
    test.faces=np.array([(3, 0, 1, 3), (3, 1, 4, 3), (3, 1, 2, 4), (3, 3, 4, 5)], dtype=[('Vertices', '<u4'), ('V0', '<u4'), ('V1', '<u4'), ('V2', '<u4')])
    
    try:
        # Add group for data on faces
        group= test.add_group("test_data_on_face", location=MDAL_DataLocation.DataOnFaces)

        # Define data  
        faces_val = [(i,) for i in range(test.faces.shape[0])]
        inverted_faces_val = [(i,) for i in reversed(range(test.faces.shape[0]))]
        data = np.array(faces_val, dtype=[('U', '<f8')])
        inverted_data = np.array(inverted_faces_val, dtype=[('U', '<f8')])

        # Add to group with 2 times
        group.add_data(data, 0.0)    
        group.add_data(inverted_data, 1.0)    
        print(f"Driver: {test.driver_name}: DOES have Write Dataset capability DataOnFaces")

        # Create a second group
        group= test.add_group("test_second_data_on_face", location=MDAL_DataLocation.DataOnFaces)  
        faces_val = [(i,) for i in range(test.faces.shape[0])]

        # Add to group with 2 times
        group.add_data(data, 0.0)
        group.add_data(inverted_data, 1.0)        
    except:
        print(f"Driver: {test.driver_name}: does not have Write Dataset capability DataOnFaces")
    
    try:
        # Add group for data on vertices
        group= test.add_group("test_data_on_vertices", location=MDAL_DataLocation.DataOnVertices)  

        # Define data  
        vertices_val = [(i,) for i in range(test.vertices.shape[0])]
        inverted_faces_val = [(i,) for i in reversed(range(test.vertices.shape[0]))]
        data = np.array(vertices_val, dtype=[('U', '<f8')])
        inverted_data = np.array(inverted_faces_val, dtype=[('U', '<f8')])

        # Add to group with 2 times
        group.add_data(data)    
        group.add_data(inverted_data, 1.0)    
        print(f"Driver: {test.driver_name}: DOES have Write Dataset capability DataOnVertices")

    except:
        print(f"Driver: {test.driver_name}: does not have Write Dataset capability DataOnVertices")

    try:
        extension = driver.filters[1:]
        test.save(f"save_test_{test.driver_name}{extension}")    
    except:
        print(f"Driver: {test.driver_name}: does not have Write mesh capability")
