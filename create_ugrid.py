from mdal import PyMesh, drivers, MDAL_DataLocation
import numpy as np

def get_ugrid_driver():
    for test_driver in drivers():
        if test_driver.name == "Ugrid":
            return test_driver
    return None

driver = get_ugrid_driver()


# Create UGRID file with data on faces
test = PyMesh(driver)

# Create vertices and faces
test.vertices=np.array([(0. , 0., 0.), (1. , 0., 0.), (2. , 0., 0.), (0.5, 1., 0.), (1.5, 1., 0.), (1. , 2., 0.)], dtype=[('X', '<f8'), ('Y', '<f8'), ('Z', '<f8')])
test.faces=np.array([(3, 0, 1, 3), (3, 1, 4, 3), (3, 1, 2, 4), (3, 3, 4, 5)], dtype=[('Vertices', '<u4'), ('V0', '<u4'), ('V1', '<u4'), ('V2', '<u4')])


# Add group for data on faces
group= test.add_group("test_data_on_face", location=MDAL_DataLocation.DataOnFaces, uri="Ugrid:created_ugrid_face_mesh.nc")

# Define data  
faces_val = [(i,) for i in range(test.faces.shape[0])]
data = np.array(faces_val, dtype=[('U', '<f8')])

reversed_faces_val = [(i,) for i in reversed(range(test.faces.shape[0]))]
reversed_data = np.array(reversed_faces_val, dtype=[('U', '<f8')])

# Add to group
nb_time = 24
for time in range(nb_time):
    if time%2:
        group.add_data(data, float(time))
    else:
        group.add_data(reversed_data, float(time))

group.save()


test.save(f"created_ugrid_face_mesh.nc") 


# Create UGRID file with data on vertices
test_vertices = PyMesh(driver)

# Create vertices and faces
test_vertices.vertices=np.array([(0. , 0., 0.), (1. , 0., 0.), (2. , 0., 0.), (0.5, 1., 0.), (1.5, 1., 0.), (1. , 2., 0.)], dtype=[('X', '<f8'), ('Y', '<f8'), ('Z', '<f8')])
test_vertices.faces=np.array([(3, 0, 1, 3), (3, 1, 4, 3), (3, 1, 2, 4), (3, 3, 4, 5)], dtype=[('Vertices', '<u4'), ('V0', '<u4'), ('V1', '<u4'), ('V2', '<u4')])


# Add group for data on faces
group= test_vertices.add_group("test_data_on_vertices", location=MDAL_DataLocation.DataOnVertices, uri="Ugrid:created_ugrid_vertices_mesh.nc")

# Define data  
vertices_val = [(i,) for i in range(test_vertices.vertices.shape[0])]
data = np.array(vertices_val, dtype=[('U', '<f8')])

reversed_vertices_val = [(i,) for i in reversed(range(test.vertices.shape[0]))]
reversed_data = np.array(reversed_vertices_val, dtype=[('U', '<f8')])

# Add to group
nb_time = 24
for time in range(nb_time):
    if time%2:
        group.add_data(data, float(time))
    else:
        group.add_data(reversed_data, float(time))

group.save()


test_vertices.save(f"created_ugrid_vertices_mesh.nc") 
